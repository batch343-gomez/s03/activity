INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01");
INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02"),
	("janesmith@gmail.com", "passwordC", "2021-01-01 03"),
	("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04"),
	("johndoe@gmail.com", "passwordE", "2021-01-01 05");

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES 
	(1, "First Code", "Hello World!", "2021-01-02 01"),
	(1, "Second Code", "Hello Earth!", "2021-01-02 02"),
	(2, "Third Code", "Welcome to Mars!", "2021-01-02 03"),
	(4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04");

SELECT * FROM posts WHERE author_id = 1;
SELECT email, datetime_created FROM users;

UPDATE posts SET content = "Hello to the people of the Earth" WHERE id = 2;

DELETE FROM users WHERE email = "johndoe@gmail.com";